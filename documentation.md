# Fast Food api v1 documentation\
https://guides.github.com/features/mastering-markdown/
## How to setup and build
1. Unzip fastfood.zip
2. ```cd fastfood```
3. ```npm install```
5. ```npm run dev```

## Changing env variables

https://davidwalsh.name/node-environment-variables


## Categories api

Get all categories from db
GET
```
http://localhost:3005/v1/categories/all
```
Add a new category to db 
POST
```
http://localhost:3005/v1/categories/add
```
Add a new category Example
```
{
    "title" = "chicken wing"
}
```
Update a Category
PUT
```
http://localhost:3005/v1/categories/Category_ID
```
Update a Category Example
```
{
    "title" = "Appetizersg"
}
```
Delete a Category
DELETE
```
http://localhost:3005/v1/categories/Category_ID
```

Add Item to db
POST
```
http://localhost:3005/v1/categories/items/add/Category_ID
```
Add Item to db example
```
{
    "title" = "Chiken Wings"
    "category" = "category id"
    "price" = 8
}
```

Update Item to db
PUT
```
http://localhost:3005/v1/categories/items/Item_ID
```
Update Item to db example
```
{
    "title" = "Chiken Wings",
    "category" = "category id",
    "price" = 8
}

Delete Item from db
DELETE
```
http://localhost:3005/v1/categories/items/Item_ID
```
Create Shopping Cart TO db
POST
```
http://localhost:3005/v1/shoppingcart/new
```
Create Shopping Cart example
```
{
"finalized": false
}
```
Get Orders after you're loggedin as Merchant GET
```
http://localhost:3005/v1/shoppingcart/orders
```
Get Orders after you're loggedin as Merchant Example
```
Create account and keep access token
then go to headers authorization and access token with Bearer in front as a value
```
Get shopping cart GET
```
http://localhost:3005/v1/shoppingcart/Shoppingcart_ID
```

Add Items to shopping cart PUT
```
http://localhost:3005/v1/shoppingcart/Shoppingcart_ID
```
Add itms Example
```
{
    "cartitems":{
        "itemid": "5f96a5229a800d576478d73f",
        "quantity": 1534623
    },
    "finalized": false
}

```

Finalize order PUT
```
http://localhost:3005/v1/shoppingcart/finalize/Shoppingcart_ID
```


Create Account TO db
POST
```
http://localhost:3005/v1/account/register
```

Create Account Example
```
{
    "email" = "your_email@email.com,
    "password" = "your_password"
}
```


Login
POST
```
http://localhost:3005/v1/account/login
```
{
    "email" = "your_email@email.com,
    "password" = "your_password"
}
Logout
POST
```
http://localhost:3005/v1/account/logout
```
Go to Headers and enter authorization and access token with Bearer in front as a value
Account
GET
```
http://localhost:3005/v1/account/me
```
Go to Headers and enter authorization and access token with Bearer in front as a value
