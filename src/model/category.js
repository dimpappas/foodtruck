import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let categorySchema = new Schema({
    title: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Category', categorySchema);