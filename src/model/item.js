import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let itemsSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        required: true
    },
    price: Number,
    quantityprice: Number
});




module.exports = mongoose.model('Item', itemsSchema);