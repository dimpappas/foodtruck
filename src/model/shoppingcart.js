import mongoose from 'mongoose';
let Schema = mongoose.Schema;


let shoppingcartSchema = new Schema({
    cartitems: [{
        title: String,
        category: String,
        price: Number,
        quantity: Number,
        required: false
    }],
    totalprice: {
        type: Number,
        required: false
    },
    finalized: {
        type: Boolean,
        required: true
    },
    modifiedOn: {
        type: Date,
        default: Date.now
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ShoppingCart', shoppingcartSchema);