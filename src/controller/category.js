import mongoose from 'mongoose';
import { Router } from 'express';
import Category from '../model/category';


export default ({config, db}) => {
    let api = Router();


api.post('/add', (req, res) => {
    let newCategory = new Category();
    newCategory.title = req.body.title;
   

    newCategory.save(err => {
        if(err) {
            res.send(err);
        }
        res.json({message: 'Category saved succesfully'});
    });
});
    api.get('/all', (req, res) => {
        Category.find({}, (err, categories) => {
          if(err) {
                res.send(err);
        }
         res.json(categories);
    });
});

api.get('/:id', (req, res) => {
    Category.findById(req.params.id, (err, category) => {
        if(err) {
            res.send(err);
        }
        res.json(category);
    });
});

api.put('/:id', (req, res) => {
    Category.findById(req.params.id, (err, category) => {
        if(err) {
            res.send(err);
        }
        category.title = req.body.title;
        category.save(err => {
            if (err) {
                res.send(err);
            }
            res.json({ message: "Category info updated"});
        });
    });
    
    api.delete('/:id', (req, res) => {
        Category.remove({
            _id: req.params.id    
        }, (err, category ) => {
            if(err) {
                res.send(err);
            }
            res.json({ message: "Category Succesfuly removed"});
        
             });
    }); 
});


        return api;

}