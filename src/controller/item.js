import { Router } from 'express';
import Item from '../model/item';
import Category from '../model/category';

export default ({
    config,
    db
}) => {
    let api = Router();

    api.post('/add/:id', (req, res) => {
        Category.findById(req.params.id, (err, category) => {
            if (err) {
                res.send(err);
            }
            let newItem = new Item();

            newItem.title = req.body.title;
            newItem.category = category._id;
            newItem.price = req.body.price;
            newItem.save((err, category) => {
                if (err) {
                    res.send(err);
                }
                res.json({
                    message: 'Item saved:'
                });
            });
        });
    });

    api.get('/all/:id', (req, res) => {
        Item.findItemCategory(req.params.id, (err, items) => {
            if (err) {
                res.send(err);
            }
            res.json(items);
        });
    });

    api.get('/:id', (req, res) => {
        Item.findById(req.params.id, (err, items) => {
            if (err) {
                res.send(err);
            }
            res.json(items);
        });
    });

    api.put('/:id', (req, res) => {
        Item.findById(req.params.id, (err, item) => {
            if (err) {
                res.send(err);
            }
            item.title = req.body.title;
            item.price = req.body.price;
            item.save((err, itemUpdate) => {
                if (err) {
                    res.send(err);
                }
                res.json({
                    message: 'Item updated'
                }, itemUpdate);
            });
        });
    });

    api.delete('/:id', (req, res) => {
        Item.remove({
            _id: req.params.id
        }, (err, item) => {
            if (err) {
                res.send(err);
            }
            res.json({
                message: "Item Succesfuly removed"
            }, item);

        });
    });
    return api;
}