import ShoppingCart from '../model/shoppingcart';
import {
    Router
} from 'express';
import {
    fixerData,
    fixerConvertor
} from "../libs/fixer"
import {
    authenticate
} from '../middleware/authMiddleware';
const fetch = require('node-fetch')

export default ({
    config,
    db
}) => {
    let api = Router();

    api.post('/new', (req, res) => {
        let newShoppingCart = new ShoppingCart();
        newShoppingCart.finalized = false
        newShoppingCart.save(err => {
            if (err) {
                res.send(err);
            }
            res.json({
                message: 'Shopping Cart created succesfully'
            });
        });
    });



    api.put('/:id', (req, res) => {
        ShoppingCart.findById(req.params.id, (err, shoppingcart) => {
            if (err) {
                res.send(err);
            }
            if (shoppingcart.finalized) {
                res.status(406).json({
                    message: "This order has been finalized and cannot be updated!",
                });
                return false;
            }
            fetch(`http://localhost:3005/v1/categories/items/${req.body.cartitems.itemid}`).then(
                function (res) {
                    return res.json()
                }
            ).then(
                function (item) {
                    let itemIndex = shoppingcart.cartitems.findIndex(
                        (p) => p._id == item._id,
                        item.quantity = req.body.cartitems.quantity,
                    )
                    if (itemIndex > -1) {
                        let productItem = shoppingcart.cartitems[itemIndex]
                        productItem.quantity = item.quantity
                        res.json({
                            message: `Item "${item.title}" quantity has beed updated to ${item.quantity}`
                        });
                    } else {
                        shoppingcart.cartitems.push(item)
                    }
                    return shoppingcart
                }
            ).then(
                function (shoppingcart) {
                    shoppingcart.save((err, items) => {
                        if (err) {
                            res.send(err);
                        }
                        res.json({
                            message: `Item "${items.title}" added to cart`
                        });
                    });
                }
            )
        });
    });

    api.put('/finalize/:id', (req, res) => {
        ShoppingCart.findById(req.params.id, (err, shoppingcart) => {
            if (err) {
                res.send(err);
            }

            function sumFunction(qty, prc) {
                return (qty * prc);
            };
            let newCartItemsArray = shoppingcart.cartitems.map(item => {

                return sumFunction(item.quantity, item.price)
            })
            let sum = newCartItemsArray.reduce(function (a, b) {
                return a + b;
            }, 0);
            shoppingcart.finalized = true
            shoppingcart.totalprice = sum
            shoppingcart.save((err, items) => {
                if (err) {
                    res.send(err);
                }
                res.json(items);
            });
        })
    })

    // api.get('/orders', authenticate, (req, res) => {
    //         ShoppingCart.find({
    //                 finalized: true
    //             }, async (err, shoppingcarts) => {
    //                 if (err) {
    //                     res.send(err);
    //                 }
    //                 if (shoppingcarts.length > 0) {
    //                     if (req.body.transaction !== "EUR") {
    //                         let tansactionValue = req.body.transaction
    //                         let universalFixerPrices = await fixerData()
    //                         if (universalFixerPrices.rates) {
    //                             for (let [key, value] of Object.entries(universalFixerPrices.rates)) {
    //                                 if (key === tansactionValue) {
    //                                     console.log(`${key}: ${value}`)
    //                                     console.log("shoppingcarts", shoppingcarts)
    //                                     const carts = shoppingcarts.map(cart => {
    //                                         let cartStrings = JSON.stringify(cart.cartitems)
    //                                         console.log('cartStrings', cartStrings)
    //                                         // console.log( Object.keys(cartStrings))})
    //                                         console.log(carts)
    //                                         //    const item = Object.keys(shoppingcarts).includes("price")
    //                                         //    console.log(item)
    //                                     })

    //                                 }
    //                             }

    //                             // {
    //                             //                                 console.log('item.price &&& value', item.price, "$%@%", value)   
    //                             //                                 item.price * value
    //                             //                                 }
    //                             //    console.log('coinToConvert, coinToConvert',coinValue)
    //                             //     shoppingcarts.cartitems.map(itemPrice => price * coinValue)
    //                         }
    //                         res.json(shoppingcarts);
    //                     } else {
    //                         res.json("No orders pending");
    //                     }
    //                 });
    //         });

        api.get('/:id', (req, res) => {
            ShoppingCart.findById(req.params.id, (err, shoppingcart) => {
                if (err) {
                    res.send(err);
                }
                res.json(shoppingcart);
            });
        });
        return api;
    }