import http, { createServer } from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import fixer from './libs/fixer';
const LocalStrategy = require('passport-local').Strategy;
const dotenv = require('dotenv');
dotenv.config()
import config from './config';
import routes from './routes';

let app = express();
app.server = http.createServer(app);
//middleware
// parse application json
app.use(bodyParser.json({
    limit: config.bodylimit
}));
//passport config
app.use(passport.initialize());
let Account = require('./model/account');
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    Account.authenticate()
));

passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// api router v1
app.use('/v1', routes);
const appPort = process.env.port
app.server.listen(appPort);
console.log(`Started on http://localhost:${appPort}`);

export default app;