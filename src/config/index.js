const dotenv = require('dotenv');
dotenv.config()

export default {
    "port": process.env.PORT,
    "mongoUrl": "mongodb://localhost:27017/fastfood-api"
}