import jwt from 'jsonwebtoken';
import expressjwt from 'express-jwt';

const dotenv = require('dotenv');
dotenv.config()

const TOKENTIME = 60*60*24*30;

let authenticate = expressjwt({ secret: process.env.SECRET, algorithms: ['HS256']});

let generateAccessToken = (req, res, next) => {
    req.token = req.token || {};
    req.token = jwt.sign({
        id: req.user.id,
    }, process.env.SECRET, {
        expiresIn: TOKENTIME
    });
    next();
}


let respond = (req, res) => {
    res.status(200).json({
        user: req.user.username,
        token: req.token
    });
}

module.exports = {
    authenticate,
    generateAccessToken,
    respond
}