import express from 'express';
import config from '../config';
import middleware from '../middleware';
import initializeDb from '../db';
import account from '../controller/account';
import Category from '../controller/category';
import Item from '../controller/item';
import ShoppingCart from '../controller/shoppingcart';

let router = express();

//connect to db
initializeDb(db => {
    //internal middleware
    router.use(middleware({ config, db}));
    //api router v1 (/v1)
    router.use('/account', account({config, db}));
    router.use('/categories', Category({config, db}));
    router.use('/categories/items', Item({config, db}));
    router.use('/shoppingcart', ShoppingCart({config, db}));
    
});

export default router;