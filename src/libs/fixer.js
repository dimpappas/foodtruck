// https://www.npmjs.com/package/fixer-api
const fixer = require("fixer-api");

fixer.set({ accessKey: "da680b1a9ea0150d85c0116a4ae985c5" });

const today = new Date();
const currentDate = () =>  today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
// IF needed await fixer.set({ accessKey: "<YOUR API KEY>" }).latest();
const fixerData = async () =>{
    const data = await fixer.latest();
    // console.log(data);
    return data
};
const fixerConvertor = async (defaultCurrency = "EUR", convertCurrencyTo ="USD", amountToConvert=100, currentDate ) =>{
    const data = await fixer.convert(defaultCurrency, convertCurrencyTo,amountToConvert, currentDate)
    // (node:11844) UnhandledPromiseRejectionWarning: Error: function_access_restricted: Access Restricted - Your current Subscription Plan does not support this API Function.
    console.log(data);
    return data
};
// console.log(fixerData());
module.exports = {fixerData,fixerConvertor }